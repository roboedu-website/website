from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import *


class SigninForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [DataRequired()])


class SignupForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [DataRequired()])
  group = StringField(validators = [DataRequired()])
  first_name = StringField(validators = [DataRequired()])
  last_name = StringField(validators = [DataRequired()])
  age = IntegerField(validators = [InputRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminUserNewForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [DataRequired()])
  group = StringField(validators = [DataRequired()])
  first_name = StringField(validators = [DataRequired()])
  last_name = StringField(validators = [DataRequired()])
  age = IntegerField(validators = [InputRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminUserChooseForm(FlaskForm):
  user_uid = SelectField(validators = [DataRequired()])


class AdminUserEditForm(FlaskForm):
  username = StringField(validators = [DataRequired()])
  password = PasswordField(validators = [])
  group = StringField(validators = [DataRequired()])
  first_name = StringField(validators = [DataRequired()])
  last_name = StringField(validators = [DataRequired()])
  age = IntegerField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminCUINewForm(FlaskForm):
  course_uid = SelectField(validators = [DataRequired()])
  user_uid = SelectField(validators = [DataRequired()])
  instructor_uid = SelectField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminCMNewForm(FlaskForm):
  course_uid = SelectField(validators = [DataRequired()])
  level = IntegerField(validators = [InputRequired()])
  i = IntegerField(validators = [InputRequired()])
  j = IntegerField(validators = [InputRequired()])
  data_type = SelectField(validators = [DataRequired()])
  data = StringField(validators = [DataRequired()])


class AdminCourseNewForm(FlaskForm):
  name = StringField(validators = [DataRequired()])
  desc = StringField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class AdminCourseChooseForm(FlaskForm):
  course_uid = SelectField(validators = [DataRequired()])


class AdminCourseEditForm(FlaskForm):
  name = StringField(validators = [DataRequired()])
  desc = StringField(validators = [DataRequired()])
  notes = StringField(validators = [DataRequired()])


class SearchForm(FlaskForm):
  query = StringField(validators = [DataRequired()])
