# Miscellaneous Utilities

import datetime


def get_utc_iso_time() -> str:
  return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()