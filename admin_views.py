from flask import url_for
from flask_admin.contrib.sqla import ModelView as SqlAModelView
from flask_login import current_user
from jinja2 import Markup
import uuid
import database


class SqlAView(SqlAModelView):
  column_display_p = True
  def is_accessible(self):
    return current_user.is_authenticated and 'admin' in current_user.group.split(' ')


class UserView(SqlAView):
  column_searchable_list = (
    database.User.username,
    database.User.user_uid,
    database.User.group,
    database.User.first_name,
    database.User.last_name,
    database.User.age,
    database.User.notes,
  )
  column_labels = dict(
    username = 'User Name',
    user_uid = 'User UID',
    group = 'Group',
    first_name = 'First Name',
    last_name = 'Last Name',
    age = 'Age',
    notes = 'Notes',
  )
  column_list = (
    database.User.username,
    database.User.user_uid,
    database.User.group,
    database.User.first_name,
    database.User.last_name,
    database.User.age,
    database.User.notes,
  )


class CourseMaterialView(SqlAView):
  column_searchable_list = (
    database.CourseMaterial.course_uid,
    database.CourseMaterial.level,
    database.CourseMaterial.i,
    database.CourseMaterial.j,
    database.CourseMaterial.data_type,
    database.CourseMaterial.data,
  )
  column_labels = dict(
    course_uid = 'Course UID',
    level = 'Level',
    i = 'Page',
    j = 'Subpage',
    data_type = 'Data Type',
    data = 'Data',
  )
  column_list = (
    database.CourseMaterial.course_uid,
    database.CourseMaterial.level,
    database.CourseMaterial.i,
    database.CourseMaterial.j,
    database.CourseMaterial.data_type,
    database.CourseMaterial.data,
  )


class CUIView(SqlAView):
  column_searchable_list = (
    database.CourseUserInstance.course_uid,
    database.CourseUserInstance.instance_uid,
    database.CourseUserInstance.user_uid,
    database.CourseUserInstance.status,
    database.CourseUserInstance.marks,
    database.CourseUserInstance.time_start,
    database.CourseUserInstance.time_end,
    database.CourseUserInstance.instructor_uid,
    database.CourseUserInstance.notes,
  )
  column_labels = dict(
    course_uid = 'Course',
    instance_uid = 'Instance UID',
    user_uid = 'User',
    status = 'Status',
    marks = 'Marks',
    time_start = 'Start Time',
    time_end = 'End Time',
    instructor_uid = 'Instructor',
    notes = 'Notes',
  )
  # https://flask-admin.readthedocs.io/en/latest/api/mod_model/#flask_admin.model.BaseModelView.column_formatters
  column_formatters = dict(
    course_uid = lambda v, c, m, p: f'{database.Course.query.filter_by(course_uid = m.course_uid).first().friendlyrepr}',
    user_uid = lambda v, c, m, p: f'{database.User.query.filter_by(user_uid = m.user_uid).first().friendlyrepr}',
    instructor_uid = lambda v, c, m, p: f'{database.User.query.filter_by(user_uid = m.instructor_uid).first().friendlyrepr}',
  )
  column_list = (
    database.CourseUserInstance.course_uid,
    database.CourseUserInstance.instance_uid,
    database.CourseUserInstance.user_uid,
    database.CourseUserInstance.status,
    database.CourseUserInstance.marks,
    database.CourseUserInstance.time_start,
    database.CourseUserInstance.time_end,
    database.CourseUserInstance.instructor_uid,
    database.CourseUserInstance.notes,
  )
  def create_model(self, form):
    """
        Create model from form.

        :param form:
            Form instance
    """
    try:
        model = self.model()
        form.populate_obj(model)
        model.instance_uid = str(uuid.uuid4())
        self.session.add(model)
        self._on_model_change(form, model, True)
        self.session.commit()
    except Exception as ex:
        if not self.handle_view_exception(ex):
            flash(gettext('Failed to create record. %(error)s', error=str(ex)), 'error')
            log.exception('Failed to create record.')

        self.session.rollback()

        return False
    else:
        self.after_model_change(form, model, True)

    return model

class CourseView(SqlAView):
  column_searchable_list = (
    database.Course.course_uid,
    database.Course.name,
    database.Course.desc,
    database.Course.notes,
  )
  column_labels = dict(
    course_uid = 'Course UID',
    name = 'Name',
    desc = 'Desc',
    notes = 'Notes,'
  )
  column_list = (
    database.Course.course_uid,
    database.Course.name,
    database.Course.desc,
    database.Course.notes,
  )


class ActionView(SqlAView):
  can_create = False
  can_edit = False
  can_delete = True
  column_searchable_list = (
    database.Action.action_time,
    database.Action.action_uid,
    database.Action.user_uid,
    database.Action.action_name,
    database.Action.action_status,
    database.Action.action_desc)
  column_labels = dict(
    action_time = 'Action Time',
    action_uid = 'Action UID',
    user_uid = 'User UID',
    action_name = 'Action Name',
    action_status = 'Action Status',
    action_desc = 'Action Description',)
  # https://flask-admin.readthedocs.io/en/latest/api/mod_model/#flask_admin.model.BaseModelView.column_formatters
  column_formatters = dict(
    action_uid = lambda v, c, m, p:
    Markup(f"<a href=\"{url_for('admin_action', action_uid = m.action_uid, prev = '/admin/action')}\">{m.action_uid}</a>"))
  column_list = (
    database.Action.action_time,
    database.Action.action_uid,
    database.Action.user_uid,
    database.Action.action_name,
    database.Action.action_status,
    database.Action.action_desc)


class IndexView(SqlAView):
  pass
